<?php
namespace App;

use App\Interfaces\InputInterface;
use App\Interfaces\ValidatorInterface;

class HolidayRoutePlanner
{
    private $input = [];

    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function setInput(InputInterface $inputHandler)
    {
        $this->input = $inputHandler->load();
    }

    public function calculate(): array
    {
        $this->validator->validate($this->input);
        $output = [];
        foreach ($this->input as $key => $holiday) {
            $output[$key] = implode('', $this->getHolidayOptimalRoute($holiday));
        }
        return $output;
    }

    private function getHolidayOptimalRoute(array $holiday): array
    {
        $route = [];
        foreach ($holiday as $destination => $dependency) {
            if ($dependency != '') {
                $route[$destination] = count($this->findDependencies($holiday, $dependency), COUNT_RECURSIVE);
            } else {
                $route[$destination] = 0;
            }
        }
        asort($route);
        return array_keys($route);
    }

    private function findDependencies($holiday, $dependency)
    {
        $result = [];
        if ($dependency != '') {
            $result[$dependency] = $this->findDependencies($holiday, $holiday[$dependency]);
        }
        return $result;
    }
}