<?php
namespace App\Interfaces;

interface InputInterface
{
    public function load(): array;
}