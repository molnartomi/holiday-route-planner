<?php
namespace App;

use App\Interfaces\ValidatorInterface;

class InputValidator implements ValidatorInterface
{
    public function validate($input)
    {
        if (empty($input)) {
            throw new \Exception('Input file is empty');
        }
        foreach ($input as $holiday) {
            $dependencies = array_filter(
                array_values($holiday),
                function($value) {
                    return $value !== '';
                }
            );
            $destinations = array_keys($holiday);
            if (!empty(array_diff($dependencies, $destinations))) {
                throw new \Exception('Invalid dependency');
            }
        }
    }
}