<?php
namespace App;

use App\Interfaces\InputInterface;

class IniInputHandler implements InputInterface
{
    private $inputFilePath;

    public function __construct(string $inputFilePath)
    {
        $this->inputFilePath = $inputFilePath;
    }

    public function load(): array
    {
        if (!is_readable($this->inputFilePath)) {
            throw new \Exception('Input file is not readable');
        }
        return parse_ini_file($this->inputFilePath, true);
    }
}