### Install and run

1. clone project

    ```
    git clone https://gitlab.com/molnartomi/holiday-route-planner.git
    ```

2. update vendor packages

    ```
   docker run --rm -v $(pwd):/app composer/composer update
   ```

3. run tests 

    ```
    docker-compose run --rm php /app/vendor/bin/phpunit /app/tests/unit/HolidayRoutePlannerTest.php 
    ```

4. run index.php

    ```
    docker-compose run --rm php /app/index.php
    ```
