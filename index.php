#!/usr/local/bin/php
<?php
require 'vendor/autoload.php';

use App\IniInputHandler;
use App\HolidayRoutePlanner;
use App\InputValidator;

$validator = new InputValidator();
$iniInputHandler = new IniInputHandler(__DIR__.'/input_example.ini');
$holidayRoutePlanner = new HolidayRoutePlanner($validator);
$holidayRoutePlanner->setInput($iniInputHandler);
$result = $holidayRoutePlanner->calculate();
print_r($result);
