<?php
namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\HolidayRoutePlanner;
use App\IniInputHandler;
use App\InputValidator;

class HolidayRoutePlannerTest extends TestCase
{
    private $validator;

    public function setUp()
    {
        $this->validator = new InputValidator();
    }

    public function testInputFileExists()
    {
        $hasException = false;
        try {
            $inputHandler = new IniInputHandler(__DIR__ . '/../../input_exampleeee.ini');
            $holidayRoutePlanner = new HolidayRoutePlanner($this->validator);
            $holidayRoutePlanner->setInput($inputHandler);
        } catch (\Exception $e) {
            $hasException = true;
        }
        $this->assertTrue($hasException);
    }

    public function testInputFileValidation()
    {
        $hasException = false;
        try {
            $inputHandler = new IniInputHandler(__DIR__ . '/../../input_example2.ini');
            $holidayRoutePlanner = new HolidayRoutePlanner($this->validator);
            $holidayRoutePlanner->setInput($inputHandler);
            $holidayRoutePlanner->calculate();
        } catch (\Exception $e) {
            $hasException = true;
        }
        $this->assertTrue($hasException);
    }

    public function testHolidayRouteCalculator()
    {
        $inputHandler = new IniInputHandler(__DIR__ . '/../../input_example.ini');
        $holidayRoutePlanner = new HolidayRoutePlanner($this->validator);
        $holidayRoutePlanner->setInput($inputHandler);
        $result = $holidayRoutePlanner->calculate();

        $this->assertEquals('x', $result['holiday1']);
        $this->assertContains($result['holiday2'], ['xyz', 'xzy', 'yxz', 'yzx', 'zxy', 'zyx']);
        $this->assertContains($result['holiday3'], ['xzy', 'zyx']);
        $this->assertGreaterThan(strpos($result['holiday3'], 'z'), strpos($result['holiday3'], 'y'));
        //$this->assertEquals('uzwxvy', $result['holiday4']);
        $this->assertGreaterThan(strpos($result['holiday4'], 'z'), strpos($result['holiday4'], 'w'));
        $this->assertGreaterThan(strpos($result['holiday4'], 'w'), strpos($result['holiday4'], 'v'));
        $this->assertGreaterThan(strpos($result['holiday4'], 'v'), strpos($result['holiday4'], 'y'));
        $this->assertGreaterThan(strpos($result['holiday4'], 'u'), strpos($result['holiday4'], 'x'));
        //$this->assertEquals('xvyzuw', $result['holiday5']);
    }
}
